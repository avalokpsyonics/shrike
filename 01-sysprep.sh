echo "You must have the Guest Additions cd insterted into the virtual drive before running this script."
echo "You must change the username of the default user in the script before running this script."
USER=user
read -rsp $"Press any key to continue, or Ctl + C to halt." -n1 key

echo ":TRUST: MOUNTING GUEST ADDITIONS CDROM."
sudo mount /dev/cdrom /media/cdrom
cd /media/cdrom

echo ":TRUST: INSTALLING GUEST ADDITIONS PREREQUISITES."
sudo apt-get install -y dkms build-essential linux-headers-$(uname -r)

echo ":TRUST: INSTALLING WINDOW SYSTEM."
sudo apt-get install -y x-window-system i3 &&

echo ":TRUST: INSTALLING GUEST ADDITIONS."
sudo ./VBoxLinuxAdditions.run

echo "The system will now reboot."
read -rsp $"Press any key to continue..." -n1 key
reboot
