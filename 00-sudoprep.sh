echo "You must edit the USER variable to point to the username of the default user."
echo ">nano 00-sudoprep.sh"
echo "You must run this as root by entering su mode like so:"
echo ">su -"
read -rsp $"Press any key to continue, or Ctl + C to halt..." -n1 key
#Change this to the username of the default user
USER="user"
echo ":TRUST: INSTALLING SUDO."
apt-get install sudo
usermod -aG sudo $USER
echo "The system will now reboot."
read -rsp $"Press any key to continue..." -n1 key
reboot
